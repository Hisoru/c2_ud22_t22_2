package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import controller.ProgramaController;
import dto.UD22Dto;
import java.awt.event.ActionListener;


//vista del menú usado para modificar campos de la base de datos del video
public class vistaUpdateVideo extends JFrame {

	
	private JPanel contentPane;
	private JTextField textFieldid;
	private JLabel lblTitle;
	private JTextField textFieldtitle;
	private JLabel lblDirector;
	private JTextField textFieldDirector;
	private JLabel lblcli_id;
	private JTextField textFieldCli_id;
	private JLabel lblTitulo;
	private ProgramaController videoController;
	private JButton btnUpdate;
	private JButton btnUpdate_1;

	public vistaUpdateVideo() {
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		setTitle("Update Cliente");
		
		JLabel lblid = new JLabel("ID");
		lblid.setBounds(25, 69, 32, 14);
		contentPane.add(lblid);
		
		textFieldid = new JTextField();
		textFieldid.setBounds(75, 66, 86, 20);
		contentPane.add(textFieldid);
		textFieldid.setColumns(10);
		
		lblTitle = new JLabel("Title");
		lblTitle.setBounds(25, 105, 45, 14);
		contentPane.add(lblTitle);
		
		textFieldtitle = new JTextField();
		textFieldtitle.setColumns(10);
		textFieldtitle.setBounds(75, 102, 86, 20);
		contentPane.add(textFieldtitle);
		
		lblDirector = new JLabel("director");
		lblDirector.setBounds(25, 141, 45, 14);
		contentPane.add(lblDirector);
		
		textFieldDirector = new JTextField();
		textFieldDirector.setColumns(10);
		textFieldDirector.setBounds(75, 138, 86, 20);
		contentPane.add(textFieldDirector);
		
		lblcli_id = new JLabel("cli_id");
		lblcli_id.setBounds(191, 105, 60, 14);
		contentPane.add(lblcli_id);
		
		textFieldCli_id = new JTextField();
		textFieldCli_id.setColumns(10);
		textFieldCli_id.setBounds(261, 102, 86, 20);
		contentPane.add(textFieldCli_id);
		
		lblTitulo = new JLabel("Update video");
		lblTitulo.setFont(new Font("Tahoma", Font.PLAIN, 17));
		lblTitulo.setBounds(146, 11, 137, 34);
		contentPane.add(lblTitulo);
		
		btnUpdate = new JButton("Update");
		btnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				UD22Dto miVideo=new UD22Dto();
				miVideo.setIdvideos(Integer.parseInt(textFieldid.getText()));
				miVideo.setTitle(textFieldtitle.getText());
				miVideo.setDirector(textFieldDirector.getText());
				miVideo.setCli_id(Integer.parseInt(textFieldCli_id.getText()));
				ProgramaController.modificarVideo(miVideo);
			}
		});
		btnUpdate.setBounds(159, 227, 89, 23);
		contentPane.add(btnUpdate);
	}
}
