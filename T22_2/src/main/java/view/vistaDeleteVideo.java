package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import controller.ProgramaController;
import dto.UD22Dto;
import java.awt.event.ActionListener;

//vista del menú para eliminar un video
public class vistaDeleteVideo extends JFrame {

	private JPanel contentPane;
	private JTextField textFieldid;
	private JLabel lblTitulo;
	private JButton btnEliminar;
	
	public vistaDeleteVideo() {
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		setTitle("Delete Cliente");
		
		JLabel lblid = new JLabel("ID");
		lblid.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblid.setBounds(36, 64, 29, 20);
		contentPane.add(lblid);
		
		textFieldid = new JTextField();
		textFieldid.setBounds(75, 66, 252, 20);
		contentPane.add(textFieldid);
		textFieldid.setColumns(10);
		
		lblTitulo = new JLabel("Delete video");
		lblTitulo.setFont(new Font("Tahoma", Font.PLAIN, 17));
		lblTitulo.setBounds(146, 11, 137, 34);
		contentPane.add(lblTitulo);
		
		btnEliminar = new JButton("Eliminar fila");
		btnEliminar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				UD22Dto miVideoDelete=new UD22Dto();
				miVideoDelete.setIdvideos(Integer.parseInt(textFieldid.getText()));
				ProgramaController.eliminarVideo(miVideoDelete);
			}
		});
		btnEliminar.setBounds(146, 193, 137, 23);
		contentPane.add(btnEliminar);
	}
}
