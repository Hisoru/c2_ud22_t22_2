package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;

import dao.*;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import controller.ProgramaController;
import dto.UD22Dto;
import java.awt.event.ActionListener;

//vista del menú que será usado para insertar un video
public class vistaInsertarVideo extends JFrame {

	private JPanel contentPane;
	private JTextField textFieldid;
	private JLabel lblTitle;
	private JTextField textFieldTitle;
	private JLabel lblDirector;
	private JTextField textFieldDirector;
	private JLabel lblDireccion;
	private JTextField textFielddireccion;
	private JLabel lblTitulo;
	private JButton btnInsertar;
	private JButton btnCancelar;
	private JButton btnInsertar_1;

	public vistaInsertarVideo() {
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		setTitle("Insertar Cliente");
		
		JLabel lblid = new JLabel("ID");
		lblid.setBounds(25, 69, 32, 14);
		contentPane.add(lblid);
		
		textFieldid = new JTextField();
		textFieldid.setBounds(75, 66, 86, 20);
		contentPane.add(textFieldid);
		textFieldid.setColumns(10);
		
		lblTitle = new JLabel("Title");
		lblTitle.setBounds(25, 105, 45, 14);
		contentPane.add(lblTitle);
		
		textFieldTitle = new JTextField();
		textFieldTitle.setColumns(10);
		textFieldTitle.setBounds(75, 102, 86, 20);
		contentPane.add(textFieldTitle);
		
		lblDirector = new JLabel("Director");
		lblDirector.setBounds(25, 141, 45, 14);
		contentPane.add(lblDirector);
		
		textFieldDirector = new JTextField();
		textFieldDirector.setColumns(10);
		textFieldDirector.setBounds(75, 138, 86, 20);
		contentPane.add(textFieldDirector);
		
		lblDireccion = new JLabel("cli_id");
		lblDireccion.setBounds(191, 69, 60, 14);
		contentPane.add(lblDireccion);
		
		textFielddireccion = new JTextField();
		textFielddireccion.setColumns(10);
		textFielddireccion.setBounds(261, 66, 86, 20);
		contentPane.add(textFielddireccion);
		
		lblTitulo = new JLabel("Insertar video");
		lblTitulo.setFont(new Font("Tahoma", Font.PLAIN, 17));
		lblTitulo.setBounds(146, 11, 137, 34);
		contentPane.add(lblTitulo);
		
		btnInsertar = new JButton("Insertar");
		btnInsertar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				UD22Dto miVideo=new UD22Dto();
				miVideo.setIdvideos(Integer.parseInt(textFieldid.getText()));
				miVideo.setTitle(textFieldTitle.getText());
				miVideo.setDirector(textFieldDirector.getText());
				miVideo.setCli_id(Integer.parseInt(textFielddireccion.getText()));

				ProgramaController.registrarVideo(miVideo);
			}
		});
		btnInsertar.setBounds(159, 194, 89, 23);
		contentPane.add(btnInsertar);
		
	}
	

}
