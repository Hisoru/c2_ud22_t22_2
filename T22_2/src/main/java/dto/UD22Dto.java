package dto;

import java.sql.Date;

public class UD22Dto {
	//Aquí crearé todos los datos que usaré para posteriormente enviarlos a la base de datos (haciendo referencia a cada campo de las tablas)
	private Integer idcliente;
	private String nombre;
	private String apellido;
	private String direccion;
	private String dni;
	private Integer fecha;
	
	private Integer idvideos;
	private String title;
	private String director;
	private Integer cli_id;
	
	//Aquí asigno los getters y los setters de cada variable
	
	public Integer getIdcliente() {
		return idcliente;
	}

	public void setIdcliente(Integer idcliente) {
		this.idcliente = idcliente;
	}
	public Integer getIdvideos() {
		return idvideos;
	}

	public void setIdvideos(Integer idvideos) {
		this.idvideos = idvideos;
	}

	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}

	public void setCli_id(Integer cli_id) {
		this.cli_id = cli_id;
	}

	public Integer getCli_id() {
		return cli_id;
	}

	public void setDirector(String director) {
		this.director = director;
	}

	public String getDirector() {
		return director;
	}


	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public Integer getFecha() {
		return fecha;
	}

	public void setFecha(Integer fecha) {
		this.fecha = fecha;
	}

}
